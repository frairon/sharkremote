# README #

This small tool allows controlling the computer with a wii controller. Communication with the wii is achieved by the 
motej library (included in the repo for compatibility reasons).

### Features ###

* multimedia control (volume, play/pause/prev/next) using the xf86 button codes via xdotool
* arrow keys
* scrolling
* mouse interaction when a IR source like from the wii (or two candles or whatever) are available.
* window switching

A complete cheat sheet is yet to be made.


## Build & Install ##

You'll need a wii controller (obviously), some bluetooth device and the following dependencies:

* jdk
* bluetooth
* bluez-utils
* libbluetooth-dev
* libopenobex1
* xdotool (for system interaction)

[SCons](http://www.scons.org) is used to build.
Just check out the repository, run `scons --deploy=deploy`.
This will create a folder `deploy` containing the main jar, all dependencies and a start script `start.sh`.