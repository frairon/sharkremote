import os
import glob

AddOption('--deploy',
        dest='deploy',
        type='string',
        nargs=1,
        action='store',
        help='specify path to deploy to')

buildDir = 'target'
deploy = GetOption('deploy')

def mkStartScript(target, source, env):
    assert deploy
    targetName = str(target[0])
    with open(targetName, 'w') as start:
        print >> start, '#!/bin/bash'
        print >> start, "cd", os.path.abspath(deploy)
        libPaths = []

        for lib in glob.glob(os.path.join(deploy, '*.jar')):
            libPaths.append(os.path.abspath(lib))
        print >> start, 'java -classpath {classpath} {main}'.format(classpath=':'.join(libPaths),
                    main='fisk.shark.remote.Remote')
    env.Execute('chmod +x ' + targetName)

def mkDesktopFile(target, source, env):
    assert deploy

    targetName = str(target[0])

    with open(targetName, 'w') as desktop:
        lines = ["[Desktop Entry]",
                 "Type=Application",
                 "Name=Shark Remote",
                 "Exec=" + os.path.abspath(os.path.join(deploy, 'start.sh')),
                 "Icon=/usr/share/icons/gnome/32x32/apps/multimedia-volumne-control.png",
                 ]
        for line in lines:
            print >> desktop, line


env = Environment(BUILDERS={'mkStartScript' : Builder(action=mkStartScript),
                              'mkDesktopFile': Builder(action=mkDesktopFile)
                              })

env.VariantDir(buildDir, 'src', duplicate=0)

# collect libs and add to java classpath
classPath = Glob('libs/*.jar') + Glob('libs/motej/*.jar')
classPathStr = map(str, classPath)
env.Append(JAVACLASSPATH=':'.join(classPathStr))

# compile sources
classes = env.Java(buildDir, 'src')

# jar them all together in one jar
finalJar = env.Jar('SharkRemote.jar', classes + ['MANIFEST.MF'])


if deploy:
    # create deployment directory
    env.Execute(Mkdir(deploy))

    # deploy all necessary files
    env.Install(deploy, finalJar)
    env.Install(deploy, "log4j.properties")

    # deploy all libraries
    for lib in classPath:
        env.Install(deploy, lib)

    # create the start script
    startScript = env.mkStartScript(os.path.join(deploy, 'start.sh'), [])

    desktopFile = env.mkDesktopFile(os.path.join(deploy, 'sharkremote.desktop'), [])
