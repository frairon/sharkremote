package fisk.shark.remote.handler;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import fisk.shark.remote.utils.CommandRepeater;
import motej.event.CoreButtonEvent;

public class ArrowKeyHandler extends CoreButtonHandler {

	private ButtonKeyMapper[] buttonHandlers = new ButtonKeyMapper[] {
			new ButtonKeyMapper(CoreButtonEvent.D_PAD_UP, KeyEvent.VK_UP),
			new ButtonKeyMapper(CoreButtonEvent.D_PAD_DOWN, KeyEvent.VK_DOWN),
			new ButtonKeyMapper(CoreButtonEvent.D_PAD_LEFT, KeyEvent.VK_LEFT),
			new ButtonKeyMapper(CoreButtonEvent.D_PAD_RIGHT, KeyEvent.VK_RIGHT) };

	@Override
	public void buttonsPressed(int pressed) {
		for (ButtonKeyMapper handler : buttonHandlers) {
			if (this.buttonInBitlist(pressed, handler.eventCode)) {
				this.robot.keyPress(handler.keyCode);
			}
		}
	}

	@Override
	public void buttonsReleased(int released) {
		for (ButtonKeyMapper handler : buttonHandlers) {
			if (this.buttonInBitlist(released, handler.eventCode)) {
				this.robot.keyRelease(handler.keyCode);
			}
		}
	}
}
