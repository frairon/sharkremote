package fisk.shark.remote.handler;

import java.awt.AWTException;
import java.awt.Robot;

import motej.event.CoreButtonEvent;

import org.apache.log4j.Logger;

import fisk.shark.remote.utils.CommandRepeater;

public class WheelHandler extends CoreButtonHandler {

	private static Logger log = Logger.getLogger(WheelHandler.class);
	private static final int WHEEL_AMP = 3;
	private Robot robot;

	private CommandRepeater scroller;

	public WheelHandler()  {
		super();
	}

	@Override
	protected void buttonsPressed(int pressed) {
		if (this.buttonInBitlist(pressed, CoreButtonEvent.D_PAD_UP)) {
			this.scroller = new CommandRepeater() {
				@Override
				protected void executeCommand() throws Exception {
					WheelHandler.this.robot.mouseWheel(-WHEEL_AMP);
				}
			};

		}

		if (this.buttonInBitlist(pressed, CoreButtonEvent.D_PAD_DOWN)) {

			this.scroller = new CommandRepeater() {
				@Override
				protected void executeCommand() throws Exception {
					WheelHandler.this.robot.mouseWheel(WHEEL_AMP);
				}
			};

		}
	}

	@Override
	protected void buttonsReleased(int released) {
		if ((this.buttonInBitlist(released, CoreButtonEvent.D_PAD_DOWN) || this
				.buttonInBitlist(released, CoreButtonEvent.D_PAD_UP))
				&& this.scroller != null) {

			this.scroller.abort();
			this.scroller = null;
		}

	}

}
