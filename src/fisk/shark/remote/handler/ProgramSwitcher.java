package fisk.shark.remote.handler;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import motej.event.CoreButtonEvent;

import org.apache.log4j.Logger;

public class ProgramSwitcher extends CoreButtonHandler {
	private static Logger log = Logger.getLogger(ProgramSwitcher.class);

	private boolean altPressed = false;

	public ProgramSwitcher() {
		super();
	}

	@Override
	protected void buttonsPressed(int pressed) {
		if (this.buttonInBitlist(pressed, CoreButtonEvent.BUTTON_HOME)
				&& this.altPressed) {
			this.pressAlt(false);
		}

		if (this.buttonInBitlist(pressed, CoreButtonEvent.BUTTON_MINUS)) {
			if (!this.altPressed) {
				this.pressAlt(true);
			}
			this.robot.keyPress(KeyEvent.VK_SHIFT);
			this.robot.keyPress(KeyEvent.VK_TAB);
		}

		if (this.buttonInBitlist(pressed, CoreButtonEvent.BUTTON_PLUS)) {
			if (!this.altPressed) {
				this.pressAlt(true);
			}
			this.robot.keyPress(KeyEvent.VK_TAB);
		}
	}

	private void pressAlt(boolean press) {
		if (press) {
			this.robot.keyPress(KeyEvent.VK_ALT);
		} else {
			this.robot.keyRelease(KeyEvent.VK_ALT);
		}
		this.altPressed = press;
	}

	@Override
	protected void buttonsReleased(int released) {
		if (this.buttonInBitlist(released, CoreButtonEvent.BUTTON_MINUS)) {
			this.robot.keyRelease(KeyEvent.VK_TAB);
			this.robot.keyRelease(KeyEvent.VK_SHIFT);
		} else if (this.buttonInBitlist(released, CoreButtonEvent.BUTTON_PLUS)) {
			this.robot.keyRelease(KeyEvent.VK_TAB);
		} else if (released != 0) {
			this.pressAlt(false);
		}
	}

}
