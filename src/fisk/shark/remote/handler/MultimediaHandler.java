package fisk.shark.remote.handler;

import java.awt.AWTException;
import java.awt.event.KeyEvent;
import java.io.IOException;

import fisk.shark.remote.utils.CommandRepeater;
import motej.event.CoreButtonEvent;

public class MultimediaHandler extends CoreButtonHandler {

	private CommandRepeater volUpRepeater;
	private CommandRepeater volDownRepeater;
	private ArrowKeyHandler arrowKeyHandler;
	private boolean arrowMode;

	public MultimediaHandler() {
		super();

		this.arrowKeyHandler = new ArrowKeyHandler();
	}

	@Override
	protected void buttonsPressed(int pressed) {
		try {
			if (this.arrowMode) {
				this.arrowKeyHandler.buttonsPressed(pressed);
			} else {

				if (this.buttonInBitlist(pressed, CoreButtonEvent.D_PAD_RIGHT)) {
					Runtime.getRuntime().exec("xdotool key XF86AudioNext");
				}
				if (this.buttonInBitlist(pressed, CoreButtonEvent.D_PAD_LEFT)) {
					Runtime.getRuntime().exec("xdotool key XF86AudioPrev");
				}
				if (this.buttonInBitlist(pressed, CoreButtonEvent.D_PAD_UP)) {
					Runtime.getRuntime().exec("xdotool key XF86AudioPlay");
				}
				if (this.buttonInBitlist(pressed, CoreButtonEvent.D_PAD_DOWN)) {
					Runtime.getRuntime().exec("xdotool key XF86AudioStop");
				}
			}

			if (this.buttonInBitlist(pressed, CoreButtonEvent.BUTTON_A)) {
				this.robot.keyPress(KeyEvent.VK_ENTER);
				this.robot.keyRelease(KeyEvent.VK_ENTER);
			}

			if (this.buttonInBitlist(pressed, CoreButtonEvent.BUTTON_HOME)) {
				Runtime.getRuntime().exec("xdotool key XF86AudioMute");
			}

			if (this.buttonInBitlist(pressed, CoreButtonEvent.BUTTON_PLUS)) {
				this.volUpRepeater = new CommandRepeater() {

					@Override
					protected void executeCommand() throws Exception {
						Runtime.getRuntime().exec(
								"xdotool key XF86AudioRaiseVolume");
					}
				};
			}

			if (this.buttonInBitlist(pressed, CoreButtonEvent.BUTTON_B)) {
				this.arrowMode = true;
			}

			if (this.buttonInBitlist(pressed, CoreButtonEvent.BUTTON_MINUS)) {

				this.volDownRepeater = new CommandRepeater() {

					@Override
					protected void executeCommand() throws Exception {
						Runtime.getRuntime().exec(
								"xdotool key XF86AudioLowerVolume");
					}
				};
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void buttonsReleased(int released) {
		if (this.buttonInBitlist(released, CoreButtonEvent.BUTTON_PLUS)
				&& this.volUpRepeater != null) {
			this.volUpRepeater.abort();
			this.volUpRepeater = null;
		}

		if (this.buttonInBitlist(released, CoreButtonEvent.BUTTON_MINUS)
				&& this.volDownRepeater != null) {
			this.volDownRepeater.abort();
			this.volDownRepeater = null;
		}

		if (this.arrowMode) {
			this.arrowKeyHandler.buttonsReleased(released);
		}

		if (this.buttonInBitlist(released, CoreButtonEvent.BUTTON_B)) {
			this.arrowMode = false;
		}

	}
}
