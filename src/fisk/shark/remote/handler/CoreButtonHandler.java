package fisk.shark.remote.handler;

import java.awt.AWTException;
import java.awt.Robot;

import motej.event.CoreButtonEvent;
import motej.event.CoreButtonListener;

abstract public class CoreButtonHandler implements CoreButtonListener {

	private int lastButtonState;
	protected Robot robot;

	protected int buttonKeys[] = new int[] { CoreButtonEvent.NO_BUTTON,
			CoreButtonEvent.D_PAD_LEFT, CoreButtonEvent.D_PAD_RIGHT,
			CoreButtonEvent.D_PAD_UP, CoreButtonEvent.D_PAD_DOWN,
			CoreButtonEvent.BUTTON_ONE, CoreButtonEvent.BUTTON_TWO,
			CoreButtonEvent.BUTTON_A, CoreButtonEvent.BUTTON_B,
			CoreButtonEvent.BUTTON_PLUS, CoreButtonEvent.BUTTON_MINUS,
			CoreButtonEvent.BUTTON_HOME, };

	protected static class ButtonKeyMapper {
		int eventCode;
		int keyCode;

		ButtonKeyMapper(int eventCode, int keyCode) {
			this.eventCode = eventCode;
			this.keyCode = keyCode;
		}
	}

	protected CoreButtonHandler() {
		try {
			this.robot = new Robot();
		} catch (AWTException e) {
			throw new RuntimeException(e);
		}
	}

	abstract protected void buttonsPressed(int pressed);

	abstract protected void buttonsReleased(int released);

	final public void buttonPressed(CoreButtonEvent evt) {

		int buttonState = evt.getButton();

		this.buttonsPressed(~this.lastButtonState & buttonState);
		this.buttonsReleased(this.lastButtonState & ~buttonState);

		this.lastButtonState = buttonState;

	}

	protected boolean buttonInBitlist(int bitlist, int button) {
		return (bitlist & button) == button;
	}

}
