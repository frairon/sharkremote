package fisk.shark.remote.handler;

import java.awt.AWTException;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Robot;

import motej.event.CoreButtonEvent;
import fisk.shark.remote.utils.CommandRepeater;

//import fisk.shark.remote.utils.MouseController;

public class ArrowMouseHandler extends CoreButtonHandler {

	private static final int STEP = 10;
	private CommandRepeater leftRepeater;

	// private MouseController mouse;

	public ArrowMouseHandler() {
		// this.mouse = new MouseController();
	}

	@Override
	protected void buttonsPressed(int pressed) {
		if (this.buttonInBitlist(pressed, CoreButtonEvent.D_PAD_LEFT)) {
			this.leftRepeater = new CommandRepeater() {

				@Override
				protected void executeCommand() throws Exception {
					Point point = new Point(100, 100);
					ArrowMouseHandler.this.moveMouse(point);
					// mouse.moveMouseRelative(-STEP, 0);
				}
			};
		}
	}

	@Override
	protected void buttonsReleased(int released) {
		if (this.buttonInBitlist(released, CoreButtonEvent.D_PAD_LEFT)
				&& this.leftRepeater != null) {
			this.leftRepeater.abort();
			this.leftRepeater = null;
		}
	}

	public void moveMouse(Point p) {
		GraphicsEnvironment ge = GraphicsEnvironment
				.getLocalGraphicsEnvironment();
		GraphicsDevice[] gs = ge.getScreenDevices();

		// Search the devices for the one that draws the specified point.
		for (GraphicsDevice device : gs) {
			GraphicsConfiguration[] configurations = device.getConfigurations();
			for (GraphicsConfiguration config : configurations) {
				Rectangle bounds = config.getBounds();
				if (bounds.contains(p)) {
					// Set point to screen coordinates.
					Point b = bounds.getLocation();
					Point s = new Point(p.x - b.x, p.y - b.y);

					try {
						Robot r = new Robot(device);
						r.mouseMove(s.x, s.y);
					} catch (AWTException e) {
						e.printStackTrace();
					}

					return;
				}
			}
		}
		// Couldn't move to the point, it may be off screen.
		return;
	}

}
