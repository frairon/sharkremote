package fisk.shark.remote.handler;

import java.awt.AWTException;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Robot;

import motej.IrPoint;
import motej.event.CoreButtonEvent;
import motej.event.CoreButtonListener;
import motej.event.IrCameraEvent;
import motej.event.IrCameraListener;
import fisk.shark.remote.utils.MouseMover;
import fisk.shark.remote.utils.MouseMover.Screen;

public class IrCamMouseHandler extends CoreButtonHandler implements
		IrCameraListener {

	private boolean mouseActivated = true;

	private MouseMover mouseMover;

	private Screen currentScreen;

	private long clicked;
	private static final long MOUSE_STEADY_MS = 100;

	public IrCamMouseHandler() {
		this.mouseMover = new MouseMover();
		this.currentScreen = this.mouseMover.getFirst();
	}

	@Override
	public void irImageChanged(IrCameraEvent evt) {
		// keep mouse steady after clicking. So we can click without drag.
		if (System.currentTimeMillis() - this.clicked < MOUSE_STEADY_MS) {
			return;
		}
		IrPoint p = evt.getIrPoint(0);
		if (this.mouseActivated) {
			this.currentScreen.move(p.x, p.y);
		}
	}

	@Override
	protected void buttonsPressed(int pressed) {
		if (this.buttonInBitlist(pressed, CoreButtonEvent.BUTTON_B)) {
			this.currentScreen = this.mouseMover.getNextScreen();
		}

		if (this.buttonInBitlist(pressed, CoreButtonEvent.BUTTON_A)) {
			this.currentScreen.clickLeft();
			this.clicked = System.currentTimeMillis();
		}
	}

	@Override
	protected void buttonsReleased(int released) {
		if (this.buttonInBitlist(released, CoreButtonEvent.BUTTON_A)) {
			this.currentScreen.releaseLeft();
		}
	}

}
