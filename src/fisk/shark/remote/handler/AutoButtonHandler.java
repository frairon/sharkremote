package fisk.shark.remote.handler;

import java.awt.AWTException;
import java.awt.Robot;
import java.util.ArrayList;
import java.util.List;

import motej.event.CoreButtonEvent;

public class AutoButtonHandler extends CoreButtonHandler {

	private List<Association> buttonAssociations;
	private Robot robot;

	public AutoButtonHandler(Association... bas) {
		try {
			this.robot = new Robot();

			this.buttonAssociations = new ArrayList<Association>();
			for (Association ba : bas) {
				this.buttonAssociations.add(ba);
			}
		} catch (AWTException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void buttonsPressed(int pressed) {
		for (Association ba : this.buttonAssociations) {
			if (this.buttonInBitlist(pressed, ba.button)) {
				this.robot.keyPress(ba.key);
			}
		}

	}

	@Override
	protected void buttonsReleased(int released) {
		for (Association ba : this.buttonAssociations) {
			if (this.buttonInBitlist(released, ba.button)) {
				this.robot.keyRelease(ba.key);
			}
		}
	}

	public static class Association {
		public int button;
		public int key;

		public Association(int button, int key) {
			this.button = button;
			this.key = key;
		}
	}

}
