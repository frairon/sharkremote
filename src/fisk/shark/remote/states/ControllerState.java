package fisk.shark.remote.states;

import java.util.EventListener;

import motej.Mote;

abstract public class ControllerState {

	public ControllerState() {

	}

	public void attachTo(Mote mote) {
		for (EventListener listener : this.getListeners()) {
			mote.addListener(listener);
		}
	}

	public void detachFrom(Mote mote) {
		for (EventListener listener : this.getListeners()) {
			mote.removeListener(listener);
		}
	}

	public void reset() {
		/** @todo reset states of pressed buttons etc. */
	}

	protected abstract EventListener[] getListeners();

}
