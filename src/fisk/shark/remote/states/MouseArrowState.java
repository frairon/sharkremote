package fisk.shark.remote.states;

import java.util.EventListener;

import fisk.shark.remote.handler.ArrowMouseHandler;

import motej.event.CoreButtonEvent;
import motej.event.CoreButtonListener;

public class MouseArrowState extends ControllerState implements
		CoreButtonListener {

	private ArrowMouseHandler mouseArrowHandler;

	public MouseArrowState() {
		super();

		this.mouseArrowHandler = new ArrowMouseHandler();
	}

	@Override
	protected EventListener[] getListeners() {

		return new EventListener[] { this };
	}

	@Override
	public void buttonPressed(CoreButtonEvent evt) {
		this.mouseArrowHandler.buttonPressed(evt);
	}

}
