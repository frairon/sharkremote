package fisk.shark.remote.states;

import java.util.EventListener;

import fisk.shark.remote.handler.ArrowKeyHandler;
import fisk.shark.remote.handler.IrCamMouseHandler;
import fisk.shark.remote.handler.ProgramSwitcher;
import fisk.shark.remote.handler.WheelHandler;
import motej.event.CoreButtonEvent;
import motej.event.CoreButtonListener;

public class MouseState extends ControllerState implements CoreButtonListener {

	private ProgramSwitcher programSwitcher;
	private ArrowKeyHandler arrowKeyHandler;
	private IrCamMouseHandler mouseHandler;

	public MouseState() {
		this.programSwitcher = new ProgramSwitcher();
		this.arrowKeyHandler = new ArrowKeyHandler();
		this.mouseHandler = new IrCamMouseHandler();
	}

	@Override
	protected EventListener[] getListeners() {
		return new EventListener[] { this, this.mouseHandler };
	}

	@Override
	public void buttonPressed(CoreButtonEvent evt) {
		this.programSwitcher.buttonPressed(evt);
		this.arrowKeyHandler.buttonPressed(evt);
	}

}
