package fisk.shark.remote.states;

import java.awt.Robot;
import java.util.EventListener;

import motej.event.CoreButtonEvent;
import motej.event.CoreButtonListener;
import fisk.shark.remote.handler.ArrowKeyHandler;
import fisk.shark.remote.handler.MultimediaHandler;

public class DefaultState extends ControllerState implements CoreButtonListener {

	private ArrowKeyHandler arrowKeys;
	private MultimediaHandler multiMediaButtons;

	public DefaultState(Robot robot) {
		super();

		this.arrowKeys = new ArrowKeyHandler();
		this.multiMediaButtons = new MultimediaHandler();
	}

	@Override
	public EventListener[] getListeners() {
		return new EventListener[] { this };
	}

	@Override
	public void buttonPressed(CoreButtonEvent evt) {
		this.arrowKeys.buttonPressed(evt);
		this.multiMediaButtons.buttonPressed(evt);
	}

}
