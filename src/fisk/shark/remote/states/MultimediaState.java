package fisk.shark.remote.states;

import java.util.EventListener;

import motej.event.CoreButtonEvent;
import motej.event.CoreButtonListener;
import fisk.shark.remote.handler.MultimediaHandler;

public class MultimediaState extends ControllerState implements
		CoreButtonListener {

	private MultimediaHandler multimediaHandler;

	public MultimediaState() {
		super();

		this.multimediaHandler = new MultimediaHandler();
	}

	@Override
	public EventListener[] getListeners() {
		return new EventListener[] { this };
	}

	@Override
	public void buttonPressed(CoreButtonEvent evt) {
		this.multimediaHandler.buttonPressed(evt);
	}
}
