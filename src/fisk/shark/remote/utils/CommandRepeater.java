package fisk.shark.remote.utils;

import org.apache.log4j.Logger;

abstract public class CommandRepeater extends Thread {

	private final static int DELAY = 500;
	private final static int REPEAT = 80;

	private static final Logger log = Logger.getLogger(CommandRepeater.class);

	private boolean stop = false;

	public CommandRepeater() {
		this.start();
	}

	@Override
	public void run() {

		try {
			log.trace("initial execution");
			this.executeCommand();
			Thread.sleep(DELAY);
			while (!this.stop) {
				log.trace("repeat execution");
				this.executeCommand();
				Thread.sleep(REPEAT);
			}
		} catch (InterruptedException e) {
			log.error("Command repetition interrupted");
			log.error(e);
		} catch (Exception e) {
			log.error("Error executing repeated command");
			log.error(e);
		}
	}

	public void abort() {
		this.stop = true;
	}

	abstract protected void executeCommand() throws Exception;

}
