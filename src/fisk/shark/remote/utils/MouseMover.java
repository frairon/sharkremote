package fisk.shark.remote.utils;

import java.awt.AWTException;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MouseMover {

	private static class SmoothingBuffer {
		private static final int SIZE = 7;

		private int curIndex;
		private boolean reset = true;
		int values[];
		{
			values = new int[SIZE];
		}

		public int next(int value) {
			if (reset) {
				Arrays.fill(this.values, value);
				this.reset = false;
				return value;
			}

			this.curIndex = (this.curIndex + 1) % SIZE;
			this.values[this.curIndex] = value;

			int sum = 0;
			for (int i : this.values) {
				sum += i;
			}

			return (int) Math.round((double) sum / (double) SIZE);
		}

		public void reset() {
			this.reset = true;
		}
	}

	private List<Screen> screens;

	private int currentScreen = 0;

	public static class Screen {
		private int width;
		private int height;

		private static final int MAX_INPUT_W = 1022;
		private static final int MAX_INPUT_H = 767;

		private SmoothingBuffer xBuffer;
		private SmoothingBuffer yBuffer;

		private Robot robot;

		public Screen(GraphicsDevice device) {
			final Rectangle bounds = device.getConfigurations()[0].getBounds();
			this.width = (int) bounds.getWidth();
			this.height = (int) bounds.getHeight();
			System.out.println("width x heigt of device");
			System.out.println(this.width + "x" + this.height);
			this.xBuffer = new SmoothingBuffer();
			this.yBuffer = new SmoothingBuffer();
			try {
				this.robot = new Robot(device);
			} catch (AWTException e) {
				throw new RuntimeException(e);
			}
		}

		public void move(int x, int y) {
			// if out of bounds (signal lost), do nothing
			if (x > MAX_INPUT_W || y == MAX_INPUT_H) {
				this.xBuffer.reset();
				this.yBuffer.reset();
				return;
			}

			x = MAX_INPUT_W - x;

			double relativeX = (double) x / (double) MAX_INPUT_W;
			double relativeY = (double) y / (double) MAX_INPUT_H;

			relativeX *= 1.2;
			relativeX -= 0.1;
			relativeY *= 1.2;
			relativeY -= 0.1;

			relativeX = Math.max(0, relativeX);
			relativeY = Math.max(0, relativeY);
			relativeX = Math.min(1, relativeX);
			relativeY = Math.min(1, relativeY);

			int screenX = (int) Math.round(relativeX * (double) this.width);

			int screenY = (int) Math.round(relativeY * (double) this.height);

			this.robot.mouseMove(this.xBuffer.next(screenX),
					this.yBuffer.next(screenY));
		}

		public void clickLeft() {
			this.robot.mousePress(InputEvent.BUTTON1_MASK);
		}

		public void releaseLeft() {
			this.robot.mouseRelease(InputEvent.BUTTON1_MASK);
		}
	}

	public MouseMover() {
		this.screens = new ArrayList<MouseMover.Screen>();

		GraphicsEnvironment ge = GraphicsEnvironment
				.getLocalGraphicsEnvironment();
		for (GraphicsDevice gd : ge.getScreenDevices()) {
			this.screens.add(new Screen(gd));
		}

	}

	public Screen getNextScreen() {
		this.currentScreen = (this.currentScreen + 1) % this.screens.size();
		System.out.println("changed screen");
		return this.screens.get(this.currentScreen);
	}

	public Screen getFirst() {
		this.currentScreen = 0;
		return this.screens.get(0);
	}
}
