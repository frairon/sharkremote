package fisk.shark.remote;

import motej.IrCameraMode;
import motej.IrCameraSensitivity;
import motej.Mote;
import motej.PostFireAction;
import motej.StatusInformationReport;
import motej.event.CoreButtonEvent;
import motej.event.StatusInformationListener;
import motej.request.ReportModeRequest;

import org.apache.log4j.Logger;

import fisk.shark.remote.handler.CoreButtonHandler;
import fisk.shark.remote.states.ControllerState;
import fisk.shark.remote.states.MouseState;
import fisk.shark.remote.states.MultimediaState;

public class Controller { // extends Thread

	private Mote mote;

	// ! object for thread synchronization
	private final ControllerState multimediaState = new MultimediaState();
	private final ControllerState mouseState = new MouseState();

	private ControllerState currentState;

	public static Controller startController(Mote mote) {
		Controller ctrl = new Controller(mote);
		ctrl.run();

		return ctrl;
	}

	private Controller(Mote mote) {

		try {
			this.mote = mote;
			this.goToState(this.multimediaState);
			this.updateLedStates(0);

		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private void goToState(final ControllerState newState) {
		final ControllerState oldState = this.currentState;

		if (newState == oldState) {
			return;
		}

		this.currentState = newState;

		mote.addPostFireAction(new PostFireAction() {

			@Override
			public void execute() {
				if (oldState != null) {
					oldState.detachFrom(mote);
				}
				newState.attachTo(mote);
			}
		});
	}

	private void updateLedStates(int led) {
		boolean[] ledStates = new boolean[4];

		// we only have 4 leds
		ledStates[led] = true;

		mote.setPlayerLeds(ledStates);
	}

	// @Override
	public void run() {
		mote.enableIrCamera(IrCameraMode.BASIC, IrCameraSensitivity.WII_LEVEL_4);

		mote.setReportMode(ReportModeRequest.DATA_REPORT_0x36);

		mote.rumble(500l);
		mote.addListener(new StatusInformationListener() {

			@Override
			public void statusInformationReceived(StatusInformationReport report) {
				System.out.println("battery: " + report.getBatteryLevel());
				System.out.println("continuous reporting: "
						+ report.isContinuousReportingEnabled());
				// System.out.println(report.);

			}
		});

		mote.addListener(new CoreButtonHandler() {

			@Override
			protected void buttonsReleased(int released) {
			}

			@Override
			protected void buttonsPressed(int pressed) {
				if (this.buttonInBitlist(pressed, CoreButtonEvent.BUTTON_ONE)) {
					Controller.this.goToState(Controller.this.multimediaState);
					Controller.this.updateLedStates(0);
				} else if (this.buttonInBitlist(pressed,
						CoreButtonEvent.BUTTON_TWO)) {
					Controller.this.goToState(Controller.this.mouseState);
					Controller.this.updateLedStates(1);
				}

			}
		});
	}
}
