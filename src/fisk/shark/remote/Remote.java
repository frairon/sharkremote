package fisk.shark.remote;

import java.awt.SystemTray;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import motej.Mote;
import motej.MoteFinder;
import motej.MoteFinderListener;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.intel.bluetooth.BlueCoveConfigProperties;

@SuppressWarnings("unused")
public class Remote {

	public static final Logger logger = Logger
			.getLogger(Remote.class.getName());
	static {
		PropertyConfigurator.configure("log4j.properties");
	}

	public static void main(String[] args) throws DiscoveryException,
			IOException {

		MoteDiscovery discover = new MoteDiscovery();

		do {
			discover.startDiscovery();
			System.out
					.println(">>Press enter to start/restart scanning for devices");
			System.in.read();
		} while (true);

	}
}
